import unittest
from lib import posts

class TestPosts(unittest.TestCase):
    def setUp(self):
        self.post = posts.Post()
        self.post_title = 'A Test Post'
        self.post_description = """This is a test description that is longer
        than fifty characters to make sure that we do not cause the tests to
        fail when running the new unit tests"""
        self.post_category = 'test_posts'
        self.post_author = 'post_test_author'

    def test_post_is_correct_type(self):
        new_post = self.post.create(self.post_title, 
                self.post_description,
                self.post_category,
                self.post_author)
        self.assertIsInstance(new_post, posts.Post)

    def test_post_fails_with_no_title(self):
        self.assertRaises(IndexError, self.post.create,
            description=self.post_description,
            category=self.post_category,
            author=self.post_author )

    def test_post_fails_with_no_description(self):
        self.assertRaises(IndexError, self.post.create,
            title=self.post_title,
            category=self.post_category,
            author=self.post_author )

    def test_post_fails_with_no_category(self):
        self.assertRaises(IndexError, self.post.create,
            title=self.post_title,
            description=self.post_description,
            author=self.post_author )

    def test_post_fails_with_no_author(self):
        self.assertRaises(IndexError, self.post.create,
            title=self.post_title,
            description=self.post_description,
            category=self.post_category )

    def test_post_has_title(self):
        new_post = self.post.create(self.post_title, 
                self.post_description,
                self.post_category,
                self.post_author)
        self.assertEqual(new_post.post_title, self.post_title)

    def test_post_has_description(self):
        new_post = self.post.create(self.post_title, 
                self.post_description,
                self.post_category,
                self.post_author)
        self.assertEqual(new_post.post_description, self.post_description)

    def test_post_has_category(self):
        new_post = self.post.create(self.post_title, 
                self.post_description,
                self.post_category,
                self.post_author)
        self.assertEqual(new_post.post_category, self.post_category)

    def test_post_has_author(self):
        new_post = self.post.create(self.post_title, 
                self.post_description,
                self.post_category,
                self.post_author)
        self.assertEqual(new_post.post_author, self.post_author)

    def test_post_title_is_at_least_10_characters(self):
        self.assertRaises(ValueError, self.post.create,
            title="short",
            description=self.post_description,
            category=self.post_category,
            author=self.post_author)

    def test_post_desc_is_at_least_50_characters(self):
        short_desc = """This is a short description"""
        self.assertRaises(ValueError, self.post.create,
            title=self.post_title,
            description=short_desc,
            category=self.post_category,
            author=self.post_author)
