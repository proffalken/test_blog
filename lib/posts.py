# posts.py
# 
# (c) Matthew Macdonald-Wallace 2016
#
#
# Posts
# =====
# 
# This class contains all the functionality required to create, edit
# and delete posts from the database.
class Post():
    def __init__(self):
        self.post_title = ""
        self.post_description = ""
        self.post_category = ""
        self.post_author = ""
    
    def create(self, title="", description="", category="", author=""):
        if title != "":
            if len(title) < 10:
                raise ValueError('Title must be at least 10 characters')
            self.post_title = title
        else:
            raise IndexError('Title was not supplied')


        if description != "":
            if len(description) < 50:
                raise ValueError('Description must be at least 50 characters')
            self.post_description = description
        else:
            raise IndexError('Description was not supplied')

        if category != "":
            self.post_category = category
        else:
            raise IndexError('Category was not supplied')

        if author != "":
            self.post_author = author
        else:
            raise IndexError('Author was not supplied')

        return self
