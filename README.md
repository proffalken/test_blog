# Test Blog

A test blog framework to provide examples of good testing practice

## Installation of dependencies

```pip install -r requirements.txt```

## Writing Tests

The tests themselves are added to `test/test_unit/test_posts.py` and the code is in `lib/posts.py`.

Note that even though we only have one class, and a single function within that class, there are
eleven tests that cover the following code branches:

* Is the new "post" object the correct object type?
* Does the create function raise the correct error if the following are ommitted:
    * The post Title
    * The post Description
    * The post Category
    * The post Author
* Does the post have the following attributes?
    * The post Title
    * The post Description
    * The post Category
    * The post Author
* Is the correct error raised if the title is too short?
* Is the correct error raised if the description is too short?

These tests exercise all of the possible branches (if statements) in the code and therefore we have 100% coverage.

The tests also exercise the smallest possible part of the test and can be tested in isolation without the need for
any databases or webservers to be started.

## running tests

```nose2```

*Note:* Test coverage results can be found in the htmlcov directory, just open index.html in a browser.
